#include "include.hpp"
#include <iostream>

int main() {
  inline_function("Hello, World\n", [](auto str) { std::cout << str; });
}
