#pragma once
#include <functional>
#include <string>

inline void inline_function(const std::string &name, auto cb) {
  cb(name);
}

inline void inline_func_functional(const std::string &name, std::function<void(std::string)> cb) {
  cb(name);
}
